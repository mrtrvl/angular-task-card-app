"use strict";
var Task = (function () {
    function Task(content, completed, show) {
        this.content = content;
        this.completed = completed;
        this.show = show;
    }
    return Task;
}());
exports.Task = Task;
//# sourceMappingURL=task.js.map